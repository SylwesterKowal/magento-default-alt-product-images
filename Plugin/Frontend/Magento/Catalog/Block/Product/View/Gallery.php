<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\MagentoDefaultAltProductImages\Plugin\Frontend\Magento\Catalog\Block\Product\View;

class Gallery
{

    public function afterGetGalleryImages(
        \Magento\Catalog\Block\Product\View\Gallery $subject,
        $result
    ) {

        if ($result instanceof \Magento\Framework\Data\Collection) {
            $product = $subject->getProduct();
            foreach ($result as $image) {
                //check if label is set
                if(!$image->getLabel()){
                    $image->setLabel($product->getName());
                }
            }
        }
        return $result;
    }
}

